//
//  ViewController.swift
//  weatherSmartphone
//
//  Created by wsr on 07/10/2019.
//  Copyright © 2019 wsr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    @IBOutlet weak var myCityTextF: UITextField!
    @IBOutlet weak var myCityLable: UILabel!
    @IBOutlet weak var temperLable: UILabel!
    
    
    var city = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if city != "" {
            let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\( city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
            
            let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
             
            Alamofire.request(url!, method: .get).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.myCityLable.text = "Мой город: \(self.city)"
                    self.temperLable.text = "Температура: \(json["main"]["temp"])℃"
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }

    @IBAction func getTemerat(_ sender: Any) {

        
    }
    
}

