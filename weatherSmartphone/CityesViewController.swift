//
//  CityesViewController.swift
//  weatherSmartphone
//
//  Created by wsr on 07/10/2019.
//  Copyright © 2019 wsr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CityesViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var cityTable: UITableView!
    
    var cityArr = [String]()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        cityTable.delegate = self
        cityTable.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cityArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = cityTable.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        
        cell.textLabel?.text = cityArr[indexPath.row]
        
        return cell
    }
    
    @IBAction func addCity(_ sender: Any) {
        let alert = UIAlertController(title: "Добавление города", message: "Введете подалуйств Ваш город", preferredStyle: .alert)
        
        alert.addTextField { textField in textField.placeholder = "Город"
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alert] _ in guard let textField =  alert?.textFields?.first else { return }
            self.cityArr.append(textField.text!)
            self.cityTable.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.cityArr.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.cityTable.reloadData()
        }
    }
    
/*
    func geTemerat(_ sender: Any) {
         
            let urlString = "http:/api.geonames.org/citiesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&lang=de&username=demo"
            
    
            let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
             
            Alamofire.request(url!, method: .get).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    for city in json["geonames"]{
                        let cName = city["name"].stringValue
                        self.cityArr.append(cName)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        self.cityTable.reloadData()
    }
    */

    
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let selectCell = cityTable.indexPathForSelectedRow
        let vc = segue.destination as! ViewController
        vc.city = cityArr[selectCell!.row]
        
    }
 
}
